# -*- coding: utf-8 -*-

import numpy as np
#from cm_ml.cmtools import *
#import numpy.ma as ma
#import matplotlib.pyplot as plt
from scipy import stats



# STATS
def ecdf(a):
    """
    * ecdf: https://stackoverflow.com/questions/3209362/how-to-plot-empirical-cdf-ecdf
    """
    x = np.sort(a)
    y = np.arange(1,len(x)+1)/float(len(x))
    return x,y




# PLOT
def test_logfit():
    # 
    n=20
    x = np.random.rand(n)
    # exp
    y = 3*np.exp(-2*x)
    #fig, ax = plt.subplots(1, 1)
    logfit(x,y,ax=None, fun='exp')
    #plt.show()
    # pow
    y=x**1.5
    logfit(x,y,ax=None, fun='pow')
    #plt.show()
 
def logfit(x,y,ax=None, fun='exp',xlabel='',ylabel=''):
    """
    linear regression & plot with y in log scale, and/or x in log scale.
    """
    # check!
    idx_y = y>0
    y=y[idx_y]
    x=x[idx_y]
    #
    y_ = np.log(y)
    #y_vec = np.logspace()
    y_vec = np.geomspace(np.min(y),np.max(y) )
    n=30
    if fun == 'exp':
        x_ = x.copy()
        x_vec = np.linspace(np.min(x),np.max(x),n  )
    elif fun =='pow':
        x_ = np.log(x)    
        x_vec = np.geomspace(np.min(x),np.max(x),n  )
    else : raise ValueError('unknown func')
    # linear regression
    slope, intercept, r_value, p_value, std_err = stats.linregress(x_,y_)
    #print('slope={} intercept={}'.format(slope, intercept))        
    #https://matplotlib.org/gallery/scales/aspect_loglog.html?highlight=loglog
    if ax is None:
        fig, ax = plt.subplots(1, 1)
    if fun == 'exp':
        #ax.set_xscale("lin")
        ax.plot(x_vec, np.exp(slope*x_vec+intercept), "r--")
    elif fun =='pow':    
        ax.set_xscale("log")
        ax.plot(x_vec, np.exp(intercept)*x_vec**slope , "r--")#intercept
    ax.set_yscale("log")
    #ax.set_adjustable("datalim")
    #ax2.plot([1, 3, 10], [1, 9, 100], "o-")
    ax.plot(x, y, "b+")
    ax.set_xlabel(xlabel)   
    ax.set_ylabel(ylabel)   
    #ax2.set_xlim(1e-1, 1e2)
    #ax2.set_ylim(1e-1, 1e3)
    #ax.set_aspect(1)
    ax.set_title("slope = %5.2f"%slope)
    #plt.show()
    return slope, intercept, r_value, p_value, std_err
